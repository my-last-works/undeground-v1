import HomePage from '@/pages/Home';
import Girls from '@/pages/Girls';
import Price from '@/pages/Price';
import Rules from '@/pages/Rules';
import Contacts from '@/pages/Contacts';

import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path : '/',
    component: HomePage
  },
  {
    path: '/girls',
    component: Girls
  },
  {
    path: '/price',
    component: Price
  },
  {
    path: '/rules',
    component: Rules
  },
  {
    path: '/contacts',
    component: Contacts
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes: routes
})

export default router;